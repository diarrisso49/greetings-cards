<?php
 ob_start();
require_once '_header.php';
require_once '_utilities.php';

// Add your code here!
function makeGreetingCard( $sender, $recipient, $template ) {

    $file_name = "$sender-$recipient.txt"   ;
    $file_name = sanitizeFileName( $file_name );
    $message = file_get_contents($template);
    $myfile = fopen("cards/$file_name", "w") ;
    fwrite($myfile, "Dear $recipient,\n\n");
    fwrite($myfile, "$message\n\n");
    fwrite($myfile, "Sincerely\n");
    fwrite($myfile, "$sender");
    fclose($myfile);

    return $file_name;
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $sender = $_POST['sender'];
    $template = $_POST['template'];
    $recipient = $_POST['recipient'];
    $file_name = makeGreetingCard($sender,$recipient,$template);

    header("Location: /card.php?name=$file_name");
    exit;
}

?>

<h1 class="my-4">Create a Greeting Card</h1>
<form method="post">
    <div class="my-3">
        <label for="sender" class="form-label">Sender Name</label>
        <label>
            <input type="text" name="sender" class="form-control" required>
        </label>
    </div>

    <div class="my-3">
        <label for="recipient" class="form-label">Recipient Name</label>
        <input type="text" name="recipient" class="form-control" required>
    </div>

    <div class="my-3">
        <label for="template" class="form-label">Template</label>
        <select name="template" class="form-select" required>
            <option value="">Choose a Template</option>
            <option value="birthday.txt">Birthday</option>
            <option value="thank_you.txt">Thank You</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary mt-1">Create Card</button>
</form>

<?php

require_once '_footer.php';
ob_end_flush();

?>
